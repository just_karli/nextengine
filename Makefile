# NextEngine Makefile

# Paths

CLEAN_PROJECTS = 3DGameEngine Engine 

default: compile

compile: 
	@echo 'compiling'
	msbuild.exe NextEngine.sln

$(CLEAN_PROJECTS):
	@echo 'cleaning projects'
	rm -rf $@/bin
	rm -rf $@/obj

clean: $(CLEAN_PROJECTS)
	rm -rf 3DGameEngine/bin
	rm -rf 3DGameEngine/obj

	rm -rf Engine/bin
	rm -rf Engine/obj
