﻿using System;
using System.Threading;
using GameEngine.Rendering;
using GameEngine.Scene;
using GameEngine.Timer;
using SharpDX.DXGI;
using SharpDX.Windows;

namespace GameEngine
{
    public class GameComponent : IDisposable
    {
        public Window Window { get; private set; }
        public IScene Scene { get; private set; }
        public D3DContext D3DContext { get; private set; }

        private bool _isRunning;

        private readonly Time _gameTime;

        public GameComponent(Window window, IScene scene)
        {
            Window = window;
            Scene = scene;

            _gameTime = new Time();
        }

        /// <summary>
        /// Starts the render loop. 
        /// </summary>
        public void Start()
        {
            if (_isRunning)
                return;

            _gameTime.FramesPerSecondUpdateEvent -= OnFramesPerSecondUpdate;
            _gameTime.FramesPerSecondUpdateEvent += OnFramesPerSecondUpdate;

            D3DContext = new D3DContext(Window.Width, Window.Height, Window.Handle);
            Scene.InitializeScene(D3DContext);

            Run();
        }

        /// <summary>
        /// Stops the render loop (and start method) by closing the window the application is running. 
        /// </summary>
        public void Stop()
        {
            if (_isRunning)
            {
                Window.Close();
            }
        }

        private void Run()
        {
            _isRunning = true;
            RenderLoop.Run(Window.Form, RenderCallback);
            _isRunning = false;
        }

        private void RenderScene()
        {
            D3DContext.Present(1, PresentFlags.None);
        }

        private void RenderCallback()
        {
            if (_isRunning)
            {
                bool renderScene = false;

                if (_gameTime.Update())
                {
                    renderScene = true;

                    Scene.HandleInput();
                    Scene.UpdateScene(_gameTime.DeltaSeconds);
                }

                if (renderScene)
                {
                    Scene.Render(D3DContext);
                    _gameTime.IncrementFpsCounter();
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }

        private void OnFramesPerSecondUpdate(object sender, FpsUpdateEvent fpsUpdateEvent)
        {
            Console.WriteLine("FPS: {0}", fpsUpdateEvent.FramesPerSecond);
        }

        public void Dispose()
        {
            _isRunning = false;
            _gameTime.FramesPerSecondUpdateEvent -= OnFramesPerSecondUpdate;

            Scene.Dispose();
            D3DContext.Dispose();
        }
    }
}
