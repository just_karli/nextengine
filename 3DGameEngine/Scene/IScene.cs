﻿using System;
using GameEngine.Rendering;
using SharpDX;

namespace GameEngine.Scene
{
    public interface IScene : IDisposable
    {
        void InitializeScene(D3DContext deviceWrapper);
        void HandleInput();
        
        void UpdateScene(double delta);
        void Render(D3DContext deviceWrapper);

        Color4 ClearColor { get; }
    }
}
