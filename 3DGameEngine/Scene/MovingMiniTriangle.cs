﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using GameEngine.Rendering;
using GameEngine.ShaderDefinitions.ConstantBuffers;
using GameEngine.VertexDefinition;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace GameEngine.Scene
{
    /// <summary>
    /// Adding a constant buffer 
    /// </summary>
    public class MovingMiniTriangle : IScene
    {
        private ShaderBytecode _vsBytecode;
        private VertexShader _vertexShader;

        private ShaderBytecode _psBytecode;
        private PixelShader _pixelShader;

        private InputLayout _layout;
        private Buffer _vbuffer;
        private Buffer _cbuffer;

        private MatrixBuffer _bufferData = new MatrixBuffer();

        public Mesh<ColoredVertex> _mesh;
        private DataStream _cbufferDataStream;

        public Color4 ClearColor { get; private set; }

        public void InitializeScene(D3DContext deviceWrapper)
        {
            ClearColor = new Color4(0);

            _vsBytecode = ShaderBytecode.CompileFromFile("Resources/color.vs", "ColorVertexshader", "vs_5_0", ShaderFlags.Debug, EffectFlags.None);
            _vertexShader = new VertexShader(deviceWrapper.Device, _vsBytecode);

            _psBytecode = ShaderBytecode.CompileFromFile("Resources/color.ps", "ColorPixelshader", "ps_5_0", ShaderFlags.Debug, EffectFlags.None);
            _pixelShader = new PixelShader(deviceWrapper.Device, _psBytecode);

            _layout = new InputLayout(deviceWrapper.Device, _vsBytecode, ColoredVertex.InputElements);
            _mesh = new Mesh<ColoredVertex>();

            _mesh.AddVertices(new List<ColoredVertex> 
            {
                new ColoredVertex(new Vector4(0.0f, 0.5f, 0.5f, 1.0f), new Color4(1.0f, 0.0f, 0.0f, 1.0f)),
                new ColoredVertex(new Vector4(0.5f, -0.5f, 0.5f, 1.0f), new Color4(0.0f, 1.0f, 0.0f, 1.0f)),
                new ColoredVertex(new Vector4(-0.5f, -0.5f, 0.5f, 1.0f), new Color4(0.0f, 0.0f, 1.0f, 1.0f))
            });

            // vbuffer
            _vbuffer = RenderUtil.CreateVertexBuffer(deviceWrapper.Device, _mesh.GetVerticesArray());
            _cbuffer = RenderUtil.CreateConstantBuffer<MatrixBuffer>(deviceWrapper.Device, Marshal.SizeOf(typeof(MatrixBuffer)));
            _cbufferDataStream = new DataStream(Marshal.SizeOf(typeof(MatrixBuffer)), true, true);

            Matrix world = _mesh.GetTransformation();
            world.Transpose();

            _bufferData.World = SharpDX.Matrix.Identity;
            //_bufferData.World = new float[]
            //{
            //    0, 0, 0, 1,
            //    0, 0, 1, 0, 
            //    0, 1, 0, 0,
            //    1, 0, 0, 0
            //};
            Vector3 cameraPosition = new Vector3(0, 0, 10);

            _bufferData.View = SharpDX.Matrix.Translation(cameraPosition);
            //_bufferData.View.translate(cameraPosition);
            
            //_bufferData.View.Transpose();

            _bufferData.Projection = SharpDX.Matrix.Identity;
            //_bufferData.Projection = Matrix.PerspectiveFovLhMatrix(3.14f / 4.0f, 800 / 600f, 0.1f, 1000f);
            //_bufferData.Projection.Transpose();
            //_bufferData.Color = new Vector4(1, 0, 0, 1);

        }

        public void HandleInput() { }

        public void UpdateScene(double delta)
        {
            
        }

        public void Render(D3DContext deviceWrapper)
        {
            var context = deviceWrapper.DeviceContext;

            context.InputAssembler.InputLayout = _layout;
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(_vbuffer, 32, 0));

            context.VertexShader.Set(_vertexShader);

            Marshal.StructureToPtr(_bufferData, _cbufferDataStream.DataPointer, false);
            var databox = new DataBox(_cbufferDataStream.DataPointer, 0, 0);

            context.UpdateSubresource(databox, _cbuffer);
            context.VertexShader.SetConstantBuffer(0, _cbuffer);

            context.PixelShader.Set(_pixelShader);

            // rasterizer
            deviceWrapper.SetViewport();
            // outputmerger
            deviceWrapper.SetRenderTarget();

            deviceWrapper.ClearRenderTarget(ClearColor);
            context.Draw(3, 0);

            deviceWrapper.Present(1, PresentFlags.None);
        }

        public void Dispose()
        {
            _vsBytecode.Dispose();
            _vertexShader.Dispose();
            _psBytecode.Dispose();
            _pixelShader.Dispose();
            _layout.Dispose();
            _vbuffer.Dispose();
            _cbuffer.Dispose();

            _cbufferDataStream.Dispose();
        }
    }
}
