﻿using GameEngine.Rendering;
using SharpDX;
using SharpDX.DXGI;

namespace GameEngine.Scene
{
    public class RainbowScene : IScene
    {
        private static int _colorModR = 1;
        private static int _colorModG = 1;
        private static int _colorModB = 1;

        private Color4 _backgroundColor;

        public Color4 ClearColor
        {
            get { return _backgroundColor; }
        }

        public void InitializeScene(D3DContext deviceWrapper)
        {
            _backgroundColor = new Color4();
        }

        public void HandleInput() { }

        public void UpdateScene(double delta)
        {
            _backgroundColor.Red += _colorModR * 0.005f;
            _backgroundColor.Green += _colorModG * 0.002f;
            _backgroundColor.Blue += _colorModB * 0.001f;

            // swap base color
            if (_backgroundColor.Red >= 1f || _backgroundColor.Red <= 0f)
                _colorModR *= -1;

            if (_backgroundColor.Green >= 1f || _backgroundColor.Green <= 0f) 
                _colorModG *= -1;

            if (_backgroundColor.Blue >= 1f || _backgroundColor.Blue <= 0f) 
                _colorModB *= -1;
        }

        public void Render(D3DContext deviceWrapper)
        {
            deviceWrapper.ClearRenderTarget(_backgroundColor);
            deviceWrapper.Present(1, PresentFlags.None);
        }

        public void Dispose() { }
    }
}
