﻿using System.IO;
using GameEngine.Rendering;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace GameEngine.Scene
{
    public class MiniTriangle : IScene
    {
        private ShaderBytecode _vsBytecode;
        private VertexShader _vertexShader;

        private ShaderBytecode _psBytecode;
        private PixelShader _pixelShader;

        private InputLayout _layout;
        private Buffer _vbuffer;

        public Color4 ClearColor { get; private set; }

        public void InitializeScene(D3DContext deviceWrapper)
        {
            ClearColor = new Color4();

            var shaderSource = Readfile("MiniTri.fx");

            _vsBytecode = ShaderBytecode.CompileFromFile("Resources/MiniTri.fx", "VS", "vs_4_0", ShaderFlags.None, EffectFlags.None);
            _vertexShader = new VertexShader(deviceWrapper.Device, _vsBytecode);

            _psBytecode = ShaderBytecode.CompileFromFile("Resources/MiniTri.fx", "PS", "ps_4_0", ShaderFlags.None, EffectFlags.None);
            _pixelShader = new PixelShader(deviceWrapper.Device, _psBytecode);

            _layout = new InputLayout(deviceWrapper.Device, _vsBytecode, new InputElement[]
            {
                new InputElement("POSITION", 0, Format.R32G32B32A32_Float, 0, 0), 
                new InputElement("COLOR", 0, Format.R32G32B32A32_Float, 16, 0), 
            });

            var vdata = new[]
            {
                new Vector4(0.0f, 0.5f, 0.5f, 1.0f), new Vector4(1.0f, 0.0f, 0.0f, 1.0f),
                new Vector4(0.5f, -0.5f, 0.5f, 1.0f), new Vector4(0.0f, 1.0f, 0.0f, 1.0f),
                new Vector4(-0.5f, -0.5f, 0.5f, 1.0f), new Vector4(0.0f, 0.0f, 1.0f, 1.0f)
            };

            // vbuffer
            _vbuffer = RenderUtil.CreateVertexBuffer(deviceWrapper.Device, vdata);
        }

        private string Readfile(string filepath)
        {
            if (File.Exists(filepath))
            {
                TextReader reader = new StreamReader(new FileStream(filepath, FileMode.Open));
                var text = reader.ReadToEnd();

                reader.Close();

                return text;
            }

            return string.Empty;
        }

        public void HandleInput() { }

        public void UpdateScene(double delta) {}

        public void Render(D3DContext deviceWrapper)
        {
            var context = deviceWrapper.DeviceContext;
            
            context.InputAssembler.InputLayout = _layout;
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(_vbuffer, 32, 0));
            
            context.VertexShader.Set(_vertexShader);
            context.PixelShader.Set(_pixelShader);

            // rasterizer
            deviceWrapper.SetViewport();
            // outputmerger
            deviceWrapper.SetRenderTarget();

            deviceWrapper.ClearRenderTarget(ClearColor);
            context.Draw(3, 0);

            deviceWrapper.Present(1, PresentFlags.None);
        }

        public void Dispose()
        {
            _vsBytecode.Dispose();
            _vertexShader.Dispose();
            _psBytecode.Dispose();
            _pixelShader.Dispose();
            _layout.Dispose();
            _vbuffer.Dispose();
        }
    }
}
