﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GameEngine.Input
{
    /// <summary>
    /// Class for getting the current keyboard state of a key.
    /// Calls underlying winapi to get the information if a key is pressed or toggled. 
    /// </summary>
    public abstract class Keyboard
    {
        [Flags]
        private enum KeyStates
        {
            None = 0,
            Down = 1,
            Toggled = 2
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern short GetKeyState(int keycode);

        private static KeyStates GetKeyState(Keys key)
        {
            KeyStates state = KeyStates.None;

            short keyState = GetKeyState((int)key);

            // If high-order bit is 1, key is down 
            // otherwise it is up
            if ((keyState & 0x8000) == 0x8000)
                state |= KeyStates.Down;

            // if low-order bit is 1, key is toggled
            if ((keyState & 1) == 1)
                state |= KeyStates.Toggled;

            return state;
        }

        public static bool IsKeyDown(Keys key)
        {
            return KeyStates.Down == (GetKeyState(key) & KeyStates.Down);
        }

        public static bool IsKeyToggled(Keys key)
        {
            return KeyStates.Toggled == (GetKeyState(key) & KeyStates.Toggled);
        }
    }
}
