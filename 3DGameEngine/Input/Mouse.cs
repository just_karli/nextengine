﻿using System.Drawing;
using System.Windows.Forms;

namespace GameEngine.Input
{
    public abstract class Mouse
    {
        public static Rectangle GetClippingRectangle()
        {
            return Cursor.Clip;
        }

        public static Cursor GetMouseCursor()
        {
            return Cursor.Current;
        }

        public static Point GetPosition()
        {
            return Cursor.Position;
        }

        public static void HideCursor()
        {
            Cursor.Hide();
        }

        public static void ShowCursor()
        {
            Cursor.Show();   
        }
    }
}
