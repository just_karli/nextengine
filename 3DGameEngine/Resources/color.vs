// globals
cbuffer MatrixBuffer : register(b0)
{
	float4x4 worldMatrix;
	float4x4 viewMatrix;
	float4x4 projMatrix;
	float4 cbColor;
};

struct VertexInput
{
	float4 position	: POSITION;
	float4 color	: COLOR;	
};

struct PixelIn
{
	float4 position : SV_POSITION;
	float4 color 	: COLOR;
};

static float4x4 Identity =
{
    { 1, 0, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 1, 0 },
    { 0, 0, 0, 1 }
};

PixelIn ColorVertexshader(VertexInput input) 
{
	PixelIn output;

	//input.position.w = 1.0f;
	output.position = input.position;

	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projMatrix);

	output.color = input.color;

	return output;
}
