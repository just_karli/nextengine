﻿using SharpDX;

namespace GameEngine
{
    public class Camera
    {
        public float FovY { get; set; }
        public float AspectRatio { get; set; }
        public float ZNear { get; set; }
        public float ZFar { get; set; }

        public Vector3 Eye { get; set; }
        public Quaternion Orientation { get; set; }

        public Camera()
        {
            Eye = new Vector3(0, 0, -10f);
        }

        public Camera(Vector3 position)
        {
            
        }

        //public Vector3 Direction
        //{
        //    get { return Vector3.(Vector3.UnitZ, Orientation).ToVector3(); }
        //}

        //public Vector3 UpVector
        //{
        //    get { return Vector3.Transform(Vector3.UnitY, Orientation).ToVector3(); }
        //}

        //public Matrix ViewMatrix
        //{
        //    get { return Matrix.LookAtLH(Eye, Eye + Direction, UpVector); }
        //}

        //public Matrix ProjectionMatrix
        //{
        //    get { return Matrix.PerspectiveFovLH(FovY, AspectRatio, ZNear, ZFar); }
        //}

        //public Matrix OldViewProjectionMatrix
        //{
        //    get { return ViewMatrix * ProjectionMatrix; }
        //}
    }
}
