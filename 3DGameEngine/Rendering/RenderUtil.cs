﻿using SharpDX.Direct3D11;

namespace GameEngine.Rendering
{
    public class RenderUtil
    {
        public static Buffer CreateVertexBuffer<T>(Device device, T[] data) where T : struct 
        {
            return Buffer.Create(device, BindFlags.VertexBuffer, data);
        }

        public static Buffer CreateIndexBuffer(Device device, int[] indices)
        {
            return Buffer.Create(device, BindFlags.IndexBuffer, indices);
        }

        public static Buffer CreateConstantBuffer<T>(Device device, int size) where T : struct
        {
            return new Buffer(device, size, ResourceUsage.Default, BindFlags.ConstantBuffer, CpuAccessFlags.None, ResourceOptionFlags.None, 0);
        }
    }
}
