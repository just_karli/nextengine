﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SharpDX.Windows;

namespace GameEngine.Rendering
{
    public class Window : IDisposable
    {
        public static Window CreateWindow(int width, int height, string title)
        {
            var renderForm = new RenderForm(title);
            renderForm.ClientSize = new Size(width, height);
            renderForm.StartPosition = FormStartPosition.CenterScreen;

            var window = new Window { Form = renderForm };

            return window;
        }

        public RenderForm Form { get; private set; }

        public int Width { get { return Form.Width; } }
        public int Height { get { return Form.Height; } }

        public IntPtr Handle { get { return Form.Handle; } }

        public void Close()
        {
            if (Form != null)
            {
                Form.Close();
            }
        }
        
        public void Dispose()
        {
            Form.Dispose();
        }
    }
}
