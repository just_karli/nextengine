﻿using System;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Device = SharpDX.Direct3D11.Device;

namespace GameEngine.Rendering
{
    public class D3DContext
    {
        private readonly Device _device;
        private readonly SwapChain _swapChain;

        public SwapChain SwapChain { get { return _swapChain; } }

        public Device Device { get { return _device; } }

        public DeviceContext DeviceContext { get; private set; }
        public RenderTargetView RenderTargetView { get; private set; }

        public ViewportF Viewport { get; set; }

        public D3DContext(int width, int height, IntPtr windowHandle)
        {
            Viewport = new ViewportF(0, 0, width, height, 0.0f, 1.0f);

            // describe the buffer
            ModeDescription bufferDescription = new ModeDescription
            {
                Width = width,
                Height = height,
                RefreshRate = new Rational(60, 1), 
                Format = Format.R8G8B8A8_UNorm
            };

            // swap chain
            SwapChainDescription swapchainDescription = new SwapChainDescription
            {
                ModeDescription = bufferDescription, 
                SampleDescription = new SampleDescription(1, 0),
                Usage = Usage.RenderTargetOutput, 
                BufferCount = 1, // 1 for double buffering
                OutputHandle = windowHandle,
                IsWindowed = true
            };

            // create device with swap chain and get device context
            Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.Debug, swapchainDescription, out _device, out _swapChain);
            DeviceContext = Device.ImmediateContext;

            // create render target
            using (Texture2D backbuffer = _swapChain.GetBackBuffer<Texture2D>(0))
            {
                RenderTargetView = new RenderTargetView(Device, backbuffer);
            }

            SetRenderTarget();
        }

        public void ClearRenderTarget(Color4 backgroundColor)
        {
            DeviceContext.ClearRenderTargetView(RenderTargetView, backgroundColor);
        }

        public void Present(int syncIntervall, PresentFlags flags)
        {
            SwapChain.Present(syncIntervall, flags);
        }

        public void SetRenderTarget()
        {
            DeviceContext.OutputMerger.SetRenderTargets(RenderTargetView);
        }

        public void SetViewport()
        {
            DeviceContext.Rasterizer.SetViewport(Viewport);
        }

        public void ResizeBuffer()
        {
            RenderTargetView.Dispose();
            SwapChain.ResizeBuffers(2, 0, 0, Format.R8G8B8A8_UNorm, SwapChainFlags.AllowModeSwitch);
            using (Texture2D backbuffer = SwapChain.GetBackBuffer<Texture2D>(0))
            {
                RenderTargetView = new RenderTargetView(Device, backbuffer);
            }

            SetRenderTarget();
        }

        public void Dispose()
        {
            _swapChain.Dispose();
            Device.Dispose();
            DeviceContext.Dispose();
            RenderTargetView.Dispose();
        }
    }
}
