﻿using System.Collections.Generic;
using System.Linq;
using SharpDX;

namespace GameEngine.Rendering
{
    public class Mesh<T> where T : struct 
    {
        public Mesh()
        {
            Position = new Vector3();
            Scale = new Vector3(1);
            Rotation = new Quaternion();
        } 

        public Vector3 Position { get; set; }
        public Vector3 Scale { get; set; }
        public Quaternion Rotation { get; set; }

        public List<T> Vertices { get; private set; }
        public List<int> Indices { get; private set; }

        public bool HasVertices
        {
            get { return Vertices != null && Vertices.Count > 0; }
        }

        public bool HasIndices
        {
            get { return Indices != null && Indices.Count > 0; }
        }

        public T[] GetVerticesArray()
        {
            return Vertices.ToArray();
        }

        public int[] GetIndicesArray()
        {
            return Indices.ToArray();
        }

        public void SetVertices(List<T> vertices)
        {
            Vertices = vertices;
        }

        public void AddVertices(List<T> vertices)
        {
            if(!HasVertices)
                SetVertices(vertices);
            else 
                vertices.AddRange(vertices);
        }

        public void SetIndices(int[] indices)
        {
            if(Indices == null)
                Indices = new List<int>();

            Indices = indices.ToList();
        }

        public void AddIndices(int[] indices)
        {
            if (!HasIndices)
                SetIndices(indices);
            else 
                Indices.AddRange(indices);
        }

        public Matrix GetTransformation()
        {
            Matrix translation = Matrix.Translation(Position);

            Matrix rotation = GetRotationMatrix();

            Matrix scale = Matrix.Scaling(Scale);

            return translation * rotation * scale;
        }

        public Matrix GetRotationMatrix()
        {
            return Matrix.RotationQuaternion(Rotation);
        }
    }
}
