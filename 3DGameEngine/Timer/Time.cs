﻿using System;

namespace GameEngine.Timer
{
    public class Time
    {
        public const double FRAME_CAP = 5000.0;
        public static readonly double FrameTimeSeconds = 1 / FRAME_CAP;

        public DateTime StartTime { get; private set; }

        public DateTime CurrentTime { get; private set; }
        public DateTime LastUpdateTime { get; private set; }

        public long UnprocessedTicks { get; private set; }
        public long FrameTimeTicks { get; private set; }

        public long DeltaTicks { get; private set; }
        public double DeltaSeconds { get { return TimeSpan.FromTicks(DeltaTicks).TotalSeconds; } }

        public int FramesPerSecond { get; private set; }

        public event EventHandler<FpsUpdateEvent> FramesPerSecondUpdateEvent;

        public Time()
        {
            StartTime = GetTime();
            CurrentTime = GetTime();
            LastUpdateTime = GetTime();

            UnprocessedTicks = 0;
            FrameTimeTicks = 0;
        }

        public static DateTime GetTime()
        {
            return DateTime.Now;
        }

        /// <summary>
        /// Collect timing information and decide if we process the data and render the scene.
        /// Only if framecap has reached to avoid unnecessary updates. Check also fps timer and throws fps event. 
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            CurrentTime = GetTime();
            var delta = CurrentTime - LastUpdateTime;
            LastUpdateTime = CurrentTime;

            UnprocessedTicks += delta.Ticks;
            FrameTimeTicks += delta.Ticks;
            
            var updateScene = UnprocessedTicks > (FrameTimeSeconds * TimeSpan.TicksPerSecond);

            if (updateScene)
            {
                UnprocessedTicks -= (long) (FrameTimeSeconds * TimeSpan.TicksPerSecond);
            }

            DeltaTicks = delta.Ticks;
            UpdateFpsCounter();
            
            return updateScene;
        }

        private void UpdateFpsCounter()
        {
            if (FrameTimeTicks > TimeSpan.TicksPerSecond)
            {
                OnFramesPerSecondUpdate();

                FramesPerSecond = 0;
                FrameTimeTicks = 0;
            }
        }

        public void IncrementFpsCounter()
        {
            FramesPerSecond++;
        }

        protected virtual void OnFramesPerSecondUpdate()
        {
            var handler = FramesPerSecondUpdateEvent;
            if (handler != null) 
                handler(this, new FpsUpdateEvent(FramesPerSecond, CurrentTime));
        }
    }
}
