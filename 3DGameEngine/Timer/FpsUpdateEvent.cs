﻿using System;

namespace GameEngine.Timer
{
    public class FpsUpdateEvent : EventArgs
    {
        public int FramesPerSecond { get; private set; }
        public DateTime GameTime { get; private set; }

        public FpsUpdateEvent(int fps, DateTime gametime)
        {
            FramesPerSecond = fps;
            GameTime = gametime;
        }
    }
}
