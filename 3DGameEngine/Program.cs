﻿using GameEngine.Rendering;
using GameEngine.Scene;

namespace GameEngine
{
    class Program
    {
        public const int WIDTH = 800;
        public const int HEIGHT = 600;

        public const string TITLE = "3D Engine";

        static void Main(string[] args)
        {
            using (var window = Window.CreateWindow(WIDTH, HEIGHT, TITLE))
            {
                GameComponent game = new GameComponent(window, new MovingMiniTriangle());
                game.Start();

                game.Dispose();    
            }
        }
    }
}
