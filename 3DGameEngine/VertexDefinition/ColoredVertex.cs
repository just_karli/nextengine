﻿using System.Runtime.InteropServices;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace GameEngine.VertexDefinition
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ColoredVertex
    {
        public Vector4 Position;
        public Color4 Color;

        public ColoredVertex(Vector4 position, Color4 color)
        {
            Position = position;
            Color = color;
        }

        public static InputElement[] InputElements
        {
            get
            {
                return new[]
                {
                    new InputElement("POSITION", 0, Format.R32G32B32A32_Float, 0, 0),
                    new InputElement("COLOR", 0, Format.R32G32B32A32_Float, 16, 0),
                };
            }
        }
    }
}
