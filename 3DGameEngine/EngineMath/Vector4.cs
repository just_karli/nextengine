﻿//using System;
//using System.IO;

//namespace GameEngine.EngineMath
//{
//    public struct Vector4
//    {
//        private float _x;
//        private float _y;
//        private float _z;
//        private float _w;

//        public float X
//        {
//            get { return _x; }
//            set { _x = value; }
//        }

//        public float Y
//        {
//            get { return _y; }
//            set { _y = value; }
//        }

//        public float Z
//        {
//            get { return _z; }
//            set { _z = value; }
//        }

//        public float W
//        {
//            get { return _w; }
//            set { _w = value; }
//        }

//        public Vector4(float unisonInitiation)
//        {
//            _x = unisonInitiation;
//            _y = unisonInitiation;
//            _z = unisonInitiation;
//            _w = 1;
//        }

//        public Vector4(float x, float y, float z)
//        {
//            _x = x;
//            _y = y;
//            _z = z;
//            _w = 1;
//        }

//        public Vector4(float x, float y, float z, float w)
//        {
//            _x = x;
//            _y = y;
//            _z = z;
//            _w = w;
//        }
        
//        public double Length()
//        {
//            return Math.Sqrt(X * X + Y * Y + Z * Z + W * W);
//        }

//        public float DotProduct(Vector4 v)
//        {
//            return X * v.X + Y * v.Y + Z * v.Z + W * v.W;
//        }

//        public Vector4 Normalize()
//        {
//            var length = Length();
//            if (Math.Abs(length) > 0.01)
//            {
//                float inverseLength = (float) (1 / length);
//                var normalizedVector = new Vector4(X * inverseLength, Y * inverseLength, Z * inverseLength, W * inverseLength);

//                return normalizedVector;
//            }

//            return new Vector4();
//        }

//        public Vector4 Add(Vector4 v)
//        {
//            return new Vector4(X + v.X, Y + v.Y, Z + v.Z, W + v.W);
//        }

//        public Vector4 Add(float scalar)
//        {
//            return new Vector4(X + scalar, Y + scalar, Z + scalar, W + scalar);
//        }

//        public Vector4 Sub(Vector4 v)
//        {
//            return new Vector4(X - v.X, Y - v.Y, Z - v.Z, W - v.W);
//        }

//        public Vector4 Sub(float scalar)
//        {
//            return new Vector4(X - scalar, Y - scalar, Z - scalar, W - scalar);
//        }

//        public Vector4 Mul(Vector4 v)
//        {
//            return new Vector4(X * v.X, Y * v.Y, Z * v.Z, W * v.W);
//        }

//        public Vector4 Mul(float scalar)
//        {
//            return new Vector4(X * scalar, Y * scalar, Z * scalar, W * scalar);
//        }

//        public Vector4 Div(Vector4 v)
//        {
//            return new Vector4(X / v.X, Y / v.Y, Z / v.Z, W / v.W);
//        }

//        public Vector4 Div(float scalar)
//        {
//            if (Math.Abs(scalar) > 0.01)
//            {
//                return new Vector4(X / scalar, Y / scalar, Z / scalar, W / scalar);
//            }

//            return new Vector4();
//        }

//        public bool Equals(Vector4 other)
//        {
//            return Math.Abs(X - other.X) <= 0.01 && Math.Abs(Y - other.Y) <= 0.01 && Math.Abs(Z - other.Z) <= 0.01 && Math.Abs(W - other.W) <= 0.01;
//        }

//        public override int GetHashCode()
//        {
//            unchecked
//            {
//                var hashCode = X.GetHashCode();
//                hashCode = (hashCode * 397) ^ Y.GetHashCode();
//                hashCode = (hashCode * 397) ^ Z.GetHashCode();
//                hashCode = (hashCode * 397) ^ W.GetHashCode();
//                return hashCode;
//            }
//        }

//        public override bool Equals(object obj)
//        {
//            if (ReferenceEquals(null, obj)) return false;
//            if (ReferenceEquals(this, obj)) return true;
//            if (obj.GetType() != GetType()) return false;
//            return Equals((Vector3) obj);
//        }

//        public override string ToString()
//        {
//            return string.Format("({0}, {1}, {2}, {3})", X, Y, Z, W);
//        }

//        public float this[int index]
//        {
//            get
//            {
//                if (index == 0)
//                    return X;

//                if (index == 1)
//                    return Y;

//                if (index == 2)
//                    return Z;

//                if (index == 3)
//                    return W;

//                throw new InvalidDataException();
//            }

//            set
//            {
//                if (index == 0)
//                   X = value;

//                if (index == 1)
//                    Y = value;

//                if (index == 2)
//                    Z = value;

//                if (index == 3)
//                    W = value;

//                if(index < 0 || index > 3)
//                    throw new InvalidDataException();
//            }
//        }
//    }
//}
