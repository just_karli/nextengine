﻿//using System;
//using System.IO;

//namespace GameEngine.EngineMath
//{
//    public class Vector3
//    {
//        public float X { get; set; }
//        public float Y { get; set; }
//        public float Z { get; set; }

//        public Vector3() { }

//        public Vector3(float unisonInitiation)
//        {
//            X = unisonInitiation;
//            Y = unisonInitiation;
//            Z = unisonInitiation;
//        }

//        public Vector3(float x, float y, float z)
//        {
//            X = x;
//            Y = y;
//            Z = z;
//        }

//        public double Length()
//        {
//            return Math.Sqrt(X * X + Y * Y + Z * Z);
//        }

//        public float DotProduct(Vector3 v)
//        {
//            return X * v.X + Y * v.Y + Z * v.Z;
//        }

//        public Vector3 Normalize()
//        {
//            var length = Length();
//            if (Math.Abs(length) > 0.01)
//            {
//                float inverseLength = (float) (1 / length);
//                var normalizedVector = new Vector3(X * inverseLength, Y * inverseLength, Z * inverseLength);

//                return normalizedVector;
//            }

//            return new Vector3();
//        }

//        public Vector3 CrossProduct(Vector3 v) 
//        {
//            var crossVector = new Vector3();
//            crossVector.X = Y * v.Z - Z * v.Y;
//            crossVector.Y = Z * v.X - X * v.Z;
//            crossVector.Z = X * v.Y - Y * v.X;

//            return crossVector;
//        }

//        public Vector3 Rotate(float angle, Vector3 axis)
//        {
//            float sine = (float) Math.Sin(-angle);
//            float cosine = (float) Math.Cos(-angle);

//            return CrossProduct(axis.Mul(sine)).Add((Mul(cosine)).Add(axis.Mul(DotProduct(axis.Mul(1 - cosine)))));
//        }

//        public Vector3 Rotate(Quaternion rotation)
//        {
//            Quaternion conjugate = rotation.Conjugate();
//            Quaternion w = rotation.Multiply(this).Multiply(conjugate);

//            return new Vector3(w._x, w._y, w._z);
//        }

//        public Vector3 Add(Vector3 v)
//        {
//            return new Vector3(X + v.X, Y + v.Y, Z + v.Z);
//        }

//        public Vector3 Add(float scalar)
//        {
//            return new Vector3(X + scalar, Y + scalar, Z + scalar);
//        }

//        public Vector3 Sub(Vector3 v)
//        {
//            return new Vector3(X - v.X, Y - v.Y, Z - v.Z);
//        }

//        public Vector3 Sub(float scalar)
//        {
//            return new Vector3(X - scalar, Y - scalar, Z - scalar);
//        }

//        public Vector3 Mul(Vector3 v)
//        {
//            return new Vector3(X * v.X, Y * v.Y, Z * v.Z);
//        }

//        public Vector3 Mul(float scalar)
//        {
//            return new Vector3(X * scalar, Y * scalar, Z * scalar);
//        }

//        public Vector3 Div(Vector3 v)
//        {
//            return new Vector3(X / v.X, Y / v.Y, Z / v.Z);
//        }

//        public Vector3 Div(float scalar)
//        {
//            if (Math.Abs(scalar) > 0.01)
//            {
//                return new Vector3(X / scalar, Y / scalar, Z / scalar);
//            }

//            return new Vector3();
//        }

//        protected bool Equals(Vector3 other)
//        {
//            return Math.Abs(X - other.X) <= 0.01 && Math.Abs(Y - other.Y) <= 0.01 && Math.Abs(Z - other.Z) <= 0.01;
//        }

//        public override int GetHashCode()
//        {
//            unchecked
//            {
//                var hashCode = X.GetHashCode();
//                hashCode = (hashCode * 397) ^ Y.GetHashCode();
//                hashCode = (hashCode * 397) ^ Z.GetHashCode();
//                return hashCode;
//            }
//        }

//        public override bool Equals(object obj)
//        {
//            if (ReferenceEquals(null, obj)) return false;
//            if (ReferenceEquals(this, obj)) return true;
//            if (obj.GetType() != GetType()) return false;
//            return Equals((Vector3) obj);
//        }

//        public override string ToString()
//        {
//            return string.Format("({0}, {1}, {2})", X, Y, Z);
//        }

//        public float this[int index]
//        {
//            get
//            {
//                if (index == 0)
//                    return X;

//                if (index == 1)
//                    return Y;

//                if (index == 2)
//                    return Z;

//                throw new InvalidDataException();
//            }

//            set
//            {
//                if (index == 0)
//                   X = value;

//                if (index == 1)
//                    Y = value;

//                if (index == 2)
//                    Z = value;

//                if(index < 0 || index > 2)
//                    throw new InvalidDataException();
//            }
//        }
//    }
//}
