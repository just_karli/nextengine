﻿// TODO not working as cbuffer, cannot transport data to gpu
//using System; 

//namespace GameEngine.EngineMath
//{
//    public struct Matrix
//    {
//        public float[] _matrix;

//        public void Init()
//        {
//            _matrix = new float[16];
//        }
//        //public Matrix()
//        //{
//        //    _matrix = new float[16];
//        //}

//        public void MakeIdendity()
//        {
//            if(_matrix == null)
//                Init();

//            for (int iRow = 0; iRow < 4; iRow++)
//            {
//                for (int iColumn = 0; iColumn < 4; iColumn++)
//                {
//                    this[iRow, iColumn] = 0;

//                    if (iRow == iColumn)
//                        this[iRow, iColumn] = 1;
//                }
//            }
//        }

//        public static Matrix MakeIdentity()
//        {
//           Matrix matrix = new Matrix();
//           matrix.Init();

//            for (int iRow = 0; iRow < 4; iRow++)
//            {
//                for (int iColumn = 0; iColumn < 4; iColumn++)
//                {
//                    matrix[iRow, iColumn] = 0;

//                    if (iRow == iColumn)
//                        matrix[iRow, iColumn] = 1;
//                } 
//            }

//            return matrix;
//        }

//        public static void MakeZero(Matrix m)
//        {
//            m.Init();

//            for (int iRow = 0; iRow < 4; iRow++)
//            {
//                for (int iColumn = 0; iColumn < 4; iColumn++)
//                {
//                    m[iRow, iColumn] = 0;
//                }
//            }
//        }

//        public Matrix Transpose()
//        {
//            Matrix transposed = new Matrix();
//            transposed.Init();

//            for (int iRow = 0; iRow < 4; iRow++)
//            {
//                for (int iColumn = 0; iColumn < 4; iColumn++)
//                {
//                    transposed[iRow, iColumn] = this[iColumn, iRow];
//                }
//            }

//            _matrix = transposed._matrix;

//            return this;
//        }

//        public static Matrix MakeTranspose(Matrix m)
//        {
//            Matrix transposed = new Matrix();
//            transposed.Init();

//            for (int iRow = 0; iRow < 4; iRow++)
//            {
//                for (int iColumn = 0; iColumn < 4; iColumn++)
//                {
//                    transposed[iRow, iColumn] = m[iColumn, iRow];
//                }
//            }

//            return transposed;
//        }

//        public void RotationX(double angle)
//        {
//            if(_matrix == null)
//                Init();

//            double radians = AngleHelper.DegreeToRadians(angle);

//            float sine = (float) Math.Sin(radians);
//            float cosine = (float) Math.Cos(radians);

//            _matrix[0] = 1.0f;
//            _matrix[1] = 0.0f;
//            _matrix[2] = 0.0f;
//            _matrix[3] = 0.0f;

//            _matrix[4] = 0.0f;
//            _matrix[5] = cosine;
//            _matrix[6] = sine;
//            _matrix[7] = 0.0f;

//            _matrix[8] = 0.0f;
//            _matrix[9] = -sine;
//            _matrix[10] = cosine;
//            _matrix[11] = 0.0f;

//            _matrix[12] = 0.0f;
//            _matrix[13] = 0.0f;
//            _matrix[14] = 0.0f;
//            _matrix[15] = 1.0f;
//        }

//        public void RotationY(double angle)
//        {
//            if (_matrix == null)
//                Init();

//            double radians = AngleHelper.DegreeToRadians(angle);

//            float sine = (float) Math.Sin(radians);
//            float cosine = (float) Math.Cos(radians);

//            _matrix[0] = cosine;
//            _matrix[1] = 0.0f;
//            _matrix[2] = -sine;
//            _matrix[3] = 0.0f;

//            _matrix[4] = 0.0f;
//            _matrix[5] = 1.0f;
//            _matrix[6] = 0.0f;
//            _matrix[7] = 0.0f;

//            _matrix[8] = sine;
//            _matrix[9] = 0.0f;
//            _matrix[10] = cosine;
//            _matrix[11] = 0.0f;

//            _matrix[12] = 0.0f;
//            _matrix[13] = 0.0f;
//            _matrix[14] = 0.0f;
//            _matrix[15] = 1.0f;
//        }

//        public void RotationZ(double angle)
//        {
//            if (_matrix == null)
//                Init();

//            double radians = AngleHelper.DegreeToRadians(angle);

//            float sine = (float) Math.Sin(radians);
//            float cosine = (float) Math.Cos(radians);

//            _matrix[0] = cosine;
//            _matrix[1] = sine;
//            _matrix[2] = 0.0f;
//            _matrix[3] = 0.0f;

//            _matrix[4] = -sine;
//            _matrix[5] = cosine;
//            _matrix[6] = 0.0f;
//            _matrix[7] = 0.0f;

//            _matrix[8] = 0.0f;
//            _matrix[9] = 0.0f;
//            _matrix[10] = 1.0f;
//            _matrix[11] = 0.0f;

//            _matrix[12] = 0.0f;
//            _matrix[13] = 0.0f;
//            _matrix[14] = 0.0f;
//            _matrix[15] = 1.0f;
//        }

//        public void Scale(float scale)
//        {
//            if (_matrix == null)
//                Init();

//            _matrix[0] = scale;
//            _matrix[1] = 0.0f;
//            _matrix[2] = 0.0f;
//            _matrix[3] = 0.0f;

//            _matrix[4] = 0.0f;
//            _matrix[5] = scale;
//            _matrix[6] = 0.0f;
//            _matrix[7] = 0.0f;

//            _matrix[8] = 0.0f;
//            _matrix[9] = 0.0f;
//            _matrix[10] = scale;
//            _matrix[11] = 0.0f;

//            _matrix[12] = 0.0f;
//            _matrix[13] = 0.0f;
//            _matrix[14] = 0.0f;
//            _matrix[15] = 1.0f;
//        }

//        public void Translate(Vector3 position)
//        {
//            Translate(position.X, position.Y, position.Z);
//        }

//        public void Translate(float x, float y, float z)
//        {
//            if (_matrix == null)
//                Init();

//            _matrix[0] = 1.0f;
//            _matrix[1] = 0.0f;
//            _matrix[2] = 0.0f;
//            _matrix[3] = 0.0f;

//            _matrix[4] = 0.0f;
//            _matrix[5] = 1.0f;
//            _matrix[6] = 0.0f;
//            _matrix[7] = 0.0f;

//            _matrix[8] = 0.0f;
//            _matrix[9] = 0.0f;
//            _matrix[10] = 1.0f;
//            _matrix[11] = 0.0f;

//            _matrix[12] = x;
//            _matrix[13] = y;
//            _matrix[14] = z;
//            _matrix[15] = 1.0f;
//        }

//        public Vector3 GetTranslation()
//        {
//            Vector3 position = new Vector3();
//            for (int i = 0; i < 3; i++)
//                position[i] = this[3, i];

//            return position;
//        }

//        public void SetTranslation(Vector3 position)
//        {
//            for (int i = 0; i < 3; i++)
//                this[3, i] = position[i];
//        }

//        public void Scale(Vector3 scalings)
//        {
//            Scale(scalings.X, scalings.Y, scalings.Z);
//        }

//        public void Scale(float x, float y, float z)
//        {
//            if (_matrix == null)
//                Init();

//            _matrix[0] = x;
//            _matrix[1] = 0.0f;
//            _matrix[2] = 0.0f;
//            _matrix[3] = 0.0f;

//            _matrix[4] = 0.0f;
//            _matrix[5] = y;
//            _matrix[6] = 0.0f;
//            _matrix[7] = 0.0f;

//            _matrix[8] = 0.0f;
//            _matrix[9] = 0.0f;
//            _matrix[10] = z;
//            _matrix[11] = 0.0f;

//            _matrix[12] = 0.0f;
//            _matrix[13] = 0.0f;
//            _matrix[14] = 0.0f;
//            _matrix[15] = 1.0f;
//        }

//        public static Matrix RotationMatrix(Vector3 forward, Vector3 up, Vector3 right)
//        {
//            Matrix rotationMatrix = new Matrix();
//            rotationMatrix.Init();

//            rotationMatrix[0, 0] = right.X;
//            rotationMatrix[0, 1] = right.Y; 
//            rotationMatrix[0, 2] = right.Z; 
//            rotationMatrix[0, 3] = 0;

//            rotationMatrix[1, 0] = up.X; 
//            rotationMatrix[1, 1] = up.Y; 
//            rotationMatrix[1, 2] = up.Z; 
//            rotationMatrix[1, 3] = 0;

//            rotationMatrix[2, 0] = forward.X;
//            rotationMatrix[2, 1] = forward.Y; 
//            rotationMatrix[2, 2] = forward.Z;
//            rotationMatrix[2, 3] = 0;

//            rotationMatrix[3, 0] = 0; 
//            rotationMatrix[3, 1] = 0;
//            rotationMatrix[3, 2] = 0; 
//            rotationMatrix[3, 3] = 1; 

//            return rotationMatrix;
//        }

//        public static Matrix PerspectiveFovLhMatrix(float fovy, float aspect, float znear, float zfar)
//        {
//            Matrix perspectiveMatrix = new Matrix();
//            perspectiveMatrix.Init();

//            float tanY = (float) Math.Tan(fovy / 2.0f);
//            if (Math.Abs(tanY) < 0.0001) tanY = 0.001f;
//            float yScale = 1.0f / tanY;

//            if (Math.Abs(aspect) < 0.0001) aspect = 0.001f;
//            float xScale = yScale / aspect;

//            perspectiveMatrix[0] = xScale;
//            perspectiveMatrix[1] = 0.0f;
//            perspectiveMatrix[2] = 0.0f;
//            perspectiveMatrix[3] = 0.0f;

//            perspectiveMatrix[4] = 0.0f;
//            perspectiveMatrix[5] = yScale;
//            perspectiveMatrix[6] = 0.0f;
//            perspectiveMatrix[7] = 0.0f;

//            perspectiveMatrix[8] = 0.0f;
//            perspectiveMatrix[9] = 0.0f;
//            perspectiveMatrix[10] = zfar / (zfar - znear);
//            perspectiveMatrix[11] = 1.0f;

//            perspectiveMatrix[12] = 0.0f;
//            perspectiveMatrix[13] = 0.0f;
//            perspectiveMatrix[14] = -znear * zfar / (zfar - znear);
//            perspectiveMatrix[15] = 0.0f;

//            return perspectiveMatrix;
//        }

//        public Matrix Mul(Matrix m)
//        {
//            Matrix product = new Matrix();
//            product.Init();

//            for (int iRow = 0; iRow < 4; iRow++)
//            {
//                for (int iColumn = 0; iColumn < 4; iColumn++)
//                {
//                    int index = GetIndex(iRow, iColumn);
//                    product[index] = 0;

//                    for (int iMid = 0; iMid < 4; iMid++)
//                        product[index] += _matrix[GetIndex(iRow, iMid)] * m[iMid, iColumn];
//                }
//            }

//            return product;
//        }

//        public float[] GetMatrix()
//        {
//            return _matrix;
//        }

//        public float this[int iRow, int iCol]
//        {
//            get { return _matrix[GetIndex(iRow, iCol)]; }
//            set { _matrix[GetIndex(iRow, iCol)] = value; }
//        }

//        public float this[int indexPosition]
//        {
//            get { return _matrix[indexPosition]; }
//            set { _matrix[indexPosition] = value; }
//        }

//        public int GetIndex(int iRow, int iCol)
//        {
//            return 4 * iRow + iCol;
//        }

//        public static Matrix operator *(Matrix first, Matrix second)
//        {
//            return first.Mul(second);
//        }
//    }
//}
