﻿//using System;

//namespace GameEngine.EngineMath
//{
//    public class Quaternion
//    {
//        public float _x { get; private set; }
//        public float _y { get; private set; }
//        public float _z { get; private set; }
//        public float _w { get; private set; }

//        public Quaternion()
//        {
//            _w = 1;
//        }

//        public Quaternion(float w, float x, float y, float z)
//        {
//            _w = w;
//            _x = x;
//            _y = y;
//            _z = z;
//        }

//        public Quaternion(Vector3 v, float angle)
//        {
//            var angleRadians = AngleHelper.DegreeToRadians(angle);

//            _w = (float) Math.Cos(angleRadians / 2);
//            _x = (float) (v.X * Math.Sin(angleRadians / 2));
//            _y = (float) (v.Y * Math.Sin(angleRadians / 2));
//            _z = (float) (v.Z * Math.Sin(angleRadians / 2));
//        }

//        /// <summary>
//        /// Quaternions are like 4d vector but with that imaginary component
//        /// </summary>
//        /// <returns></returns>
//        public double Length()
//        {
//            return Math.Sqrt(_x * _x + _y * _y + _z * _z + _w * _w);
//        }

//        public Quaternion Normalize()
//        {
//            var length = (float) Length();

//            _x /= length;
//            _y /= length;
//            _z /= length;
//            _w /= length;

//            return this;
//        }

//        /// <summary>
//        /// Conjugate in complex numbers, multiply all imaginary numbers with -1.
//        /// </summary>
//        /// <returns></returns>
//        public Quaternion Conjugate()
//        {
//            return new Quaternion(_w, -_x, -_y, -_z);
//        }

//        public Vector3 GetVector()
//        {
//            return new Vector3(_x, _y, _z);
//        }

//        public Quaternion Multiply(Quaternion r)
//        {
//            //double wMath = _w * r._w - GetVector().DotProduct(r.GetVector());
//            //Vector3 v = GetVector().Mul(r._w).Add(r.GetVector().Mul(_w)).Add( GetVector().CrossProduct(r.GetVector()));
//            //var q = new Quaternion(wMath, v.X, v.Y, v.Z);

//            float w  = _w * r._w - (_x * r._x) - (_y * r._y) - (_z * r._z);
//            float x = _x * r._w + (_w * r._x) + (_y * r._z) - (_z * r._y);
//            float y = _y * r._w + (_w * r._y) + (_z * r._x) - (_x * r._z);
//            float z = _z * r._w + (_w * r._z) + (_x * r._y) - (_y * r._x);

//            return new Quaternion(w, x, y, z);
//        }

//        public Quaternion Multiply(Vector3 v)
//        {
//            //Quaternion p = new Quaternion(v, 0);
//            //Quaternion qp = Multiply(v);
//            //Quaternion qInv = Conjugate();
//            //return qp.Multiply(qInv);

//            //Vector3 vcv = GetVector().CrossProduct(v);
//            //Vector3 result = v.Add(vcv.Mul(2 * _w)).Add(v.CrossProduct(vcv).Mul(2));
//            // return result;

//            float w = -_x * v.X - _y * v.Y - _z * v.Z;
//            float x = _w * v.X + _y * v.Z - _z * v.Y;
//            float y = _w * v.Y + _z * v.X - _x * v.Z;
//            float z = _w * v.Z + _x * v.Y - _y * v.X;

//            return new Quaternion(w, x, y, z);
//        }

//        public Matrix GetRotationMatrix()
//        {
//            Vector3 forward = GetForwardDirection();
//            Vector3 up = GetUpDirection();
//            Vector3 right = GetRightDirection();

//            return Matrix.RotationMatrix(forward, up, right);
//        }

//        public Vector3 GetForwardDirection()
//        {
//            var forwardDirection = new Vector3();
//            forwardDirection.X = 2.0f * (_x * _z - _w * _y);
//            forwardDirection.Y = 2.0f * (_y * _z + _w * _x);
//            forwardDirection.Z = 1.0f - 2.0f * (_x * _x + _y * _y);

//            return forwardDirection;
//        }

//        public Vector3 GetUpDirection()
//        {
//            var upDirection = new Vector3();
//            upDirection.X = 2.0f * (_x * _y + _w * _z);
//            upDirection.Y = 1.0f - 2.0f * (_x * _x + _z * _z);
//            upDirection.Z = 2.0f * (_y * _z - _w * _x);

//            return upDirection;
//        }

//        public Vector3 GetRightDirection()
//        {
//            var rightDirection = new Vector3();
//            rightDirection.X = 1.0f - 2.0f * (_y * _y + _z * _z);
//            rightDirection.Y = 2.0f * (_x * _y - _w * _z);
//            rightDirection.Z = 2.0f * (_w * _z + _w * _y);

//            return rightDirection;
//        }
//    }
//}
