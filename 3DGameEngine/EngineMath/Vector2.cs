﻿using System;

namespace GameEngine.EngineMath
{
    public class Vector2
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Vector2() { }

        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double Length()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public double DotProduct(Vector2 v)
        {
            return X * v.X + Y * v.Y;
        }

        public Vector2 Normalize()
        {
            var inverseLength = 1 / Length();
            if (Math.Abs(inverseLength) > 0.01f)
            {
                var normalizedVector = new Vector2(X * inverseLength, Y * inverseLength);
                return normalizedVector;
            }

            return new Vector2();
        }

        public Vector2 Rotate(float angle)
        {
            double radians = AngleHelper.DegreeToRadians(angle);
            double cosine = Math.Cos(radians);
            double sine = Math.Sin(radians);

            var rotationVector = new Vector2( X * cosine - Y * sine, X * sine + Y * cosine);

            return rotationVector;
        }

        public Vector2 Add(Vector2 v)
        {
            return new Vector2(X + v.X, Y + v.Y);
        }

        public Vector2 Add(float scalar)
        {
            return new Vector2(X + scalar, Y + scalar);
        }

        public Vector2 Sub(Vector2 v)
        {
            return new Vector2(X - v.X, Y - v.Y);
        }

        public Vector2 Sub(float scalar)
        {
            return new Vector2(X - scalar, Y - scalar);
        }

        public Vector2 Mul(Vector2 v)
        {
            return new Vector2(X * v.X, Y * v.Y);
        }

        public Vector2 Mul(float scalar)
        {
            return new Vector2(X * scalar, Y * scalar);
        }

        public Vector2 Div(Vector2 v)
        {
            return new Vector2(X / v.X, Y / v.Y);
        }

        public Vector2 Div(float scalar)
        {
            if (Math.Abs(scalar) > 0.01)
            {
                return new Vector2(X / scalar, Y / scalar);
            }

            return new Vector2();
        }

        protected bool Equals(Vector2 other)
        {
            return Math.Abs(X - other.X) <= 0.01 && Math.Abs(Y - other.Y) <= 0.01;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Vector2) obj);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
    }
}
