﻿using System;

namespace GameEngine.EngineMath
{
    public class AngleHelper
    {
        public static double DegreeToRadians(double angle)
        {
            double radians = Math.PI * 2 * (angle / 360.0);
            return radians;
        }

        public static double RadiansToAngle(double radians)
        {
            return radians * (180 / Math.PI);
        }
    }
}
