﻿namespace GameEngine.EngineMath
{
    public struct Color4 
    {
        private float _x;
        private float _y;
        private float _z;
        private float _w;

        public float Red
        {
            get { return _x; }
            set { _x = value; }
        }

        public float Green
        {
            get { return _y; }
            set { _y = value; }
        }

        public float Blue
        {
            get { return _z; }
            set { _z = value; }
        }

        public float Alpha
        {
            get { return _w; }
            set { _w = value; }
        }

        public Color4(float unisonInitiation)
        {
            _x = _y = _z = unisonInitiation;
            _w = 1;
        }

        public Color4(float red, float green, float blue, float alpha)
        {
            _x = red;
            _y = green;
            _z = blue;
            _w = alpha;
        }

        public void Clamp()
        {
            if (Red > 1.0f) Red = 1.0f;
            if (Red < 0.0f) Red = 0.0f;

            if (Green > 1.0f) Green = 1.0f;
            if (Green < 0.0f) Green = 0.0f;

            if (Blue > 1.0f) Blue = 1.0f;
            if (Blue < 0.0f) Blue = 0.0f;

            if (Alpha > 1.0f) Alpha = 1.0f;
            if (Alpha < 0.0f) Alpha = 0.0f;
        }

        public uint ToArgb()
        {
            uint intColor = 0;

            Clamp();

            intColor += (uint)(255 * Blue);
            intColor += (uint)(255 * Green) << 8;
            intColor += (uint)(255 * Red) << 16;
            intColor += (uint)(255 * Alpha) << 24;

            return intColor;
        }

        public uint ToRgba()
        {
            uint intColor = 0;

            Clamp();

            intColor += (uint)(255 * Alpha);
            intColor += (uint)(255 * Blue) << 8;
            intColor += (uint)(255 * Green) << 16;
            intColor += (uint)(255 * Red) << 24;

            return intColor;
        }

        public void FromArgb(uint color)
        {
            Red = ((color & 0x00ff0000) >> 16) / 255.0f;
            Green = ((color & 0x0000ff00) >> 8) / 255.0f;
            Blue = (color & 0x000000ff) / 255.0f;

            Alpha = ((color & 0xff000000) >> 24) / 255.0f;
        }
    }
}
