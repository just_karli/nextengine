﻿using System.Runtime.InteropServices;
using GameEngine.EngineMath;

namespace GameEngine.ShaderDefinitions.ConstantBuffers
{
    [StructLayout(LayoutKind.Explicit, Size = 192)]
    public struct MatrixBuffer
    {
        [FieldOffset(0)]
        public SharpDX.Matrix World;

        [FieldOffset(64)]
        public SharpDX.Matrix View;

        [FieldOffset(128)]
        public SharpDX.Matrix Projection;

        //[FieldOffset(192)]
        //public Vector4 Color;
    }
}
