﻿using System.Drawing;
using System.Windows.Forms;
using SharpDX;
using SharpDX.DXGI;
using SharpDX.Windows;

namespace SharpDxWindow
{
    class Program
    {
        private const int WIDTH = 800;
        private const int HEIGHT = 600;

        private static int _colorModR = 1;
        private static int _colorModG = 1;
        private static int _colorModB = 1;
        
        private static RenderForm _renderForm;
        private static DxInit _d3dContext;

        private static Color4 _backgroundColor;

        static void Main(string[] args)
        {
            // create the render form
            _renderForm = new RenderForm("My First Game");
            _renderForm.ClientSize = new Size(WIDTH, HEIGHT);
            _renderForm.StartPosition = FormStartPosition.CenterScreen;

            // initialize d3d
            _d3dContext = new DxInit(WIDTH, HEIGHT, _renderForm.Handle);

            InitializeScene();

            // run the loop
            RenderLoop.Run(_renderForm, RenderCallback);

            _d3dContext.Dispose();
        }

        private static void InitializeScene()
        {
            // load models and other stuff here later before we start the game loop
            _backgroundColor = new Color4();
        }

        /// <summary>
        /// Update the positioning of models, react on game input and do some other calculations 
        /// which have to be done to make them ready  to be drawn in RenderScene().
        /// </summary>
        private static void UpdateScene()
        {
            // update rainbow colored background
            _backgroundColor.Red += _colorModR * 0.005f;
            _backgroundColor.Green += _colorModG * 0.002f;
            _backgroundColor.Blue += _colorModB * 0.001f;

            // swap base color
            if (_backgroundColor.Red >= 1f || _backgroundColor.Red <= 0f) _colorModR *= -1;
            if (_backgroundColor.Green >= 1f || _backgroundColor.Green <= 0f) _colorModG *= -1;
            if (_backgroundColor.Blue >= 1f || _backgroundColor.Blue <= 0f) _colorModB *= -1;
        }

        /// <summary>
        /// Draw all models and other things as set up in UpdateScene()
        /// </summary>
        private static void DrawScene()
        {
            _d3dContext.ClearRenderTarget(_backgroundColor);
            _d3dContext.Present(1, PresentFlags.None);
        }

        private static void RenderCallback()
        {
            // here the game update and drawing code will be placed soon
            UpdateScene();
            DrawScene();
        }
    }
}
