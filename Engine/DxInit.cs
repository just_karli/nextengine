﻿using System;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Device = SharpDX.Direct3D11.Device;

namespace SharpDxWindow
{
    public class DxInit : IDisposable
    {
        private readonly Device _device;
        private readonly SwapChain _swap_chain;

        public SwapChain SwapChain { get { return _swap_chain; } }

        public Device Device { get { return _device; } }

        public DeviceContext DeviceContext { get; private set; }
        public RenderTargetView RenderTargetView { get; private set; }

        public DxInit(int width, int height, IntPtr windowHandle)
        {
            // describe the buffer
            ModeDescription bufferDescription = new ModeDescription
            {
                Width = width,
                Height = height,
                RefreshRate = new Rational(60, 1), 
                Format = Format.R8G8B8A8_UNorm
            };

            // swap chain
            SwapChainDescription swapchainDescription = new SwapChainDescription
            {
                ModeDescription = bufferDescription, 
                SampleDescription = new SampleDescription(1, 0),
                Usage = Usage.RenderTargetOutput, 
                BufferCount = 1, // 1 for double buffering
                OutputHandle = windowHandle,
                IsWindowed = true
            };

            // create device with swap chain and get device context
            Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.None, swapchainDescription, out _device, out _swap_chain);
            DeviceContext = Device.ImmediateContext;

            // create render target
            using (Texture2D backbuffer = _swap_chain.GetBackBuffer<Texture2D>(0))
            {
                RenderTargetView = new RenderTargetView(Device, backbuffer);
            }

            DeviceContext.OutputMerger.SetRenderTargets(RenderTargetView);
        }

        public void ClearRenderTarget(Color4 backgroundColor)
        {
            DeviceContext.ClearRenderTargetView(RenderTargetView, backgroundColor);
        }

        public void Present(int syncIntervall, PresentFlags flags)
        {
            SwapChain.Present(syncIntervall, flags);
        }

        public void Dispose()
        {
            _swap_chain.Dispose();
            Device.Dispose();
            DeviceContext.Dispose();
            RenderTargetView.Dispose();
        }
    }
}
