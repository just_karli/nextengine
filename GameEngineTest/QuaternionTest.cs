﻿//using System;
//using GameEngine.EngineMath;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace GameEngineTest
//{
//    [TestClass]
//    public class QuaternionTest
//    {
//        [TestMethod]
//        public void RotateRightVector()
//        {
//            float degrees = 0;
//            Vector3 vector = new Vector3(1, 0, 0);

//            var rotationQuaternion = new Quaternion(vector, degrees);
//            Assert.AreEqual(rotationQuaternion._w, 1);
//            Assert.AreEqual(rotationQuaternion.GetVector(), new Vector3());

//            degrees = 90;
//            rotationQuaternion = new Quaternion(vector, degrees);
//            Assert.AreEqual(rotationQuaternion._w, Math.Sqrt(2) / 2, 0.01);
//            Assert.AreEqual(rotationQuaternion._x, Math.Sqrt(2) / 2, 0.01);
//            Assert.AreEqual(rotationQuaternion._y, 0);
//            Assert.AreEqual(rotationQuaternion._z, 0);

//            degrees = 180;
//            rotationQuaternion = new Quaternion(vector, degrees);
//            Assert.AreEqual(rotationQuaternion._w, 0, 0.01);
//            Assert.AreEqual(rotationQuaternion._x, 1);
//            Assert.AreEqual(rotationQuaternion._y, 0);
//            Assert.AreEqual(rotationQuaternion._z, 0);
//        }

//        [TestMethod]
//        public void RotateUpVector()
//        {
//            float degrees = 90;
//            Vector3 vector = new Vector3(0, 1, 0);

//            var rotationQuaternion = new Quaternion(vector, degrees);

//            Assert.AreEqual(rotationQuaternion._w, Math.Sqrt(2) / 2, 0.01);
//            Assert.AreEqual(rotationQuaternion._x, 0);
//            Assert.AreEqual(rotationQuaternion._y, Math.Sqrt(2) / 2, 0.01);   
//            Assert.AreEqual(rotationQuaternion._z, 0);

//            degrees = 180;
//            rotationQuaternion = new Quaternion(vector, degrees);
//            Assert.AreEqual(rotationQuaternion._w, 0, 0.01);
//            Assert.AreEqual(rotationQuaternion._x, 0);
//            Assert.AreEqual(rotationQuaternion._y, 1);
//            Assert.AreEqual(rotationQuaternion._z, 0);
//        }

//        [TestMethod]
//        public void QuaternionMultiplication()
//        {
//            Quaternion q1 = new Quaternion(new Vector3(0, 1, 0), 90);
//            Quaternion q2 = new Quaternion(new Vector3(1, 0, 0), 45);

//            // first rotation last 
//            Quaternion q3 = q2.Multiply(q1);

//            Assert.AreEqual(q1._w, 0.707107, 0.01);
//            Assert.AreEqual(q1._x, 0, 0.01);
//            Assert.AreEqual(q1._y, 0.707107, 0.01);
//            Assert.AreEqual(q1._z, 0, 0.01);

//            Assert.AreEqual(q2._w, 0.923880, 0.01);
//            Assert.AreEqual(q2._x, 0.382683, 0.01);
//            Assert.AreEqual(q2._y, 0, 0.01);
//            Assert.AreEqual(q2._z, 0, 0.01);

//            Assert.AreEqual(q3._w, 0.653281, 0.01);
//            Assert.AreEqual(q3._x, 0.270598, 0.01);
//            Assert.AreEqual(q3._y, 0.653281, 0.01);
//            Assert.AreEqual(q3._z, 0.270598, 0.01);
//        }

//        [TestMethod]
//        public void QuaternionVectorMultiplication()
//        {
//            Quaternion q1 = new Quaternion(new Vector3(0, 1, 0), 90);
//            Quaternion q2 = new Quaternion(new Vector3(1, 0, 0), 45);

//            Quaternion q3 = q2.Multiply(q1);

//            // rotate (1, 0, 0) with q3
//            Vector3 qv1 = new Vector3(1, 0, 0).Rotate(q3);

//            Assert.AreEqual(qv1.X, 0, 0.01);
//            Assert.AreEqual(qv1.Y, 0.707107, 0.01);
//            Assert.AreEqual(qv1.Z, -0.707107, 0.01);

//            // rotate (1, 0, 0) with q1
//            //Vector3 qv2 = q1.Multiply(new Vector3(1, 0, 0));
//            Vector3 qv2 = new Vector3(1, 0, 0).Rotate(q1);

//            Assert.AreEqual(qv2.X, 0, 0.01);
//            Assert.AreEqual(qv2.Y, 0, 0.01);
//            Assert.AreEqual(qv2.Z, -1, 0.01);

//            // rotate (0, 0, -1) with q2
//            //Vector3 qv3 = q2.Multiply(qv2.GetVector());
//            Vector3 qv3 = qv2.Rotate(q2);

//            Assert.AreEqual(qv3.X, 0, 0.01);
//            Assert.AreEqual(qv3.Y, 0.707107, 0.01);
//            Assert.AreEqual(qv3.Z, -0.707107, 0.01);
//        }
//    }
//}
